<?php
	header('Access-Control-Allow-Origin: *');
	error_reporting(0);
	mb_language('uni');
	mb_internal_encoding('UTF-8');

	/* 將時區設置為台灣 */
	date_default_timezone_set("ROC");
	
	/* 紀錄結果是否成功 */
	$m_ResponseSuccess = false;
	
	//紀錄是否有收到資料
	$m_HasData = false;

	if(isset($_GET[keyword])){
		//檢查POST
		$m_Message = $_GET[keyword];
		$m_HasData = true;
	}

	//如果有收到資料，再做檔案處理
	if($m_HasData){
		//定義檔案名稱為使用者UUID
		$c_FileName = 'keywords.md';	//will be named as "uuid-2014-01-21.txt"

		//嘗試開啟檔案，如找不到則建立
		//目前還有無法建立Unicode檔名的問題，因此傳入之檔名一定要是英數
		$c_FileHandle = fopen($c_FileName, 'a+');

		//最後要寫入的資料
		$c_FinalData = "\n".$m_Message;

		//寫入檔案
		fwrite($c_FileHandle, $c_FinalData);

		//關閉檔案
		fclose($c_FileHandle);

		//回應成功文字作收
		$m_ResponseSuccess = true;
	}
	
	/* 輸出結果 */
	if($m_ResponseSuccess){
		echo '1';
	}else{
		echo '-1';
	}
?>