console.log('domain check is running...');

function download( _domain ){
    return new RSVP.Promise(function(resolve, reject){
        $.get('http://www.whois365.com/tw/domain/' + _domain)
        .done(function(_dom){
            resolve( { dom: _dom, domain: _domain } );
        })
        .fail(function(){
            reject();
        })
    });
}

function test( param ){
    return new RSVP.Promise(function(resolve, reject){
        if( param.dom.match(/可以註冊/) || param.dom.match(/is available for purchase/) ){
            resolve(param.domain);
        }else{
            reject(param.domain);
        }
    });
}

for( var i = 0 ; i < dic.length ; i++ )
{
    var _domainToTest =  dic[i] + '.in';

    download( _domainToTest )
    .then(test)
    .then(function(domain){
        // console.log(domain);
        $.get('http://localhost/domain-stream/writer.php?keyword=' + domain);
        console.error('o ' + domain);
    })
    .catch(function(domain){
        console.log('x ' + domain);
    });
}